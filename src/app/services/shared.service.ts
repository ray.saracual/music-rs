import { Injectable } from '@angular/core';
import { environment} from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import * as dataArtists from "./artists.json";


const URL = environment.URL;

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient) { }
  

  login(user) {
    return this.http.post<any>(URL + 'login', user);
     }



  register(user) {
    return this.http.post(URL + 'api/user/create-user', user);
     }


getArtists(){
  return dataArtists.items;
}


     getNewRelease(){
      return this.http.get("https://platzi-music-api.now.sh/browse/new-releases");
     }


     getArtistsTopTracks(id) {
      return this.http.get<any>('https://platzi-music-api.now.sh/artists/' + id + '/top-tracks?country=CO');
     }


     getAlbumTracks(albumId) {
      return this.http.get<any>('https://platzi-music-api.now.sh/albums/' + albumId + '/tracks?country=CO');
     }
}
