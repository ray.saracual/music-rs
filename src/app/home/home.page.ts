import { Component } from "@angular/core";
import { SharedService } from "../services/shared.service";
import { ModalController } from "@ionic/angular";
import { SongsModalPage } from "../songs-modal/songs-modal.page";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  newTime;
  slideOption = {
    initialSlide: 2,
    slidesPerView: 4,
    centeredSlides: true,
    speed: 400
  };

  artists: any[] = [];
  songs: any = [];
  albums: any[] = [];
  song: {
    preview_url: string;
    playing: boolean;
    name: string;
  } = {
    preview_url: "",
    playing: false,
    name: ""
  };
  currentSong: HTMLAudioElement;

  constructor(
    private sharedService: SharedService,
    private modalController: ModalController
  ) {}

  // se carga cuando el usuario navega a este componente
  ionViewDidEnter() {
    this.sharedService.getNewRelease().subscribe(
      (res: any) => {
        this.artists = this.sharedService.getArtists();
        this.songs = res.albums.items.filter(e => e.album_type === "single");
        this.albums = res.albums.items.filter(e => e.album_type === "album");
      },
      error => {
        console.log(error);
      }
    );
  }

  showSong(artist) {
    this.sharedService.getArtistsTopTracks(artist.id).subscribe(
      songArtist => {
        console.log(songArtist);
        this.modalSong(songArtist, artist);
      },
      error => {
        console.log(error);
      }
    );
  }

  showAlbumsSongs(album) {
    this.sharedService.getAlbumTracks(album.id).subscribe(
      songsAlbum => {
        this.modalSong(songsAlbum, album.artists[0]);
      },
      error => {
        console.log(error);
      }
    );
  }

  async modalSong(songArtist, artist) {
    const modal = await this.modalController.create({
      component: SongsModalPage,
      componentProps: {
        songs: songArtist.tracks ? songArtist.tracks : songArtist.items,

        artist: artist.name
      }
    });

    modal.onDidDismiss().then(dataReturn => {
      this.song = dataReturn.data;
    });

    return await modal.present();
  }

  play() {
    this.currentSong = new Audio(this.song.preview_url);
    this.currentSong.play();
    this.currentSong.addEventListener("timeupdate", () => {
      this.newTime =
        (this.currentSong.currentTime * (this.currentSong.duration / 10)) / 100;
    });
    this.song.playing = true;
  }
  pause() {
    this.currentSong.pause();
    this.song.playing = false;
  }

  parseTime(time: number) {
    if (time) {
      const partTime = parseInt(time.toString().split(".")[0], 10);
      let minutes = Math.floor(partTime / 60).toString();

      if (minutes.length == 1) {
        minutes = "0" + minutes;
      }

      let seconds = (partTime % 60).toString();
      if (seconds.length == 1) {
        seconds = "0" + seconds;
      }

      return minutes + ":" + seconds;
    }
  }
}
