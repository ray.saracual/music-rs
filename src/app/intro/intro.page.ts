import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {


  slideOptions = {
    initialSlide: 0,
    slidePerView: 1,
    centeredSlides: true,
    speed: 400
  };

  slides = [
    {
      imageSrc:"assets/img/logo.png",
      imageAlt:'logo',
      title: "Escucha tu musica",
      subtitle: "Donde quiera que este",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Error ducimus sit beatae.",
      icon: "play"
    },
    { imageSrc:"assets/img/logo.png",

      imageAlt:'logo',
      title: "Escucha tu musica",
      subtitle: "Donde quiera que este",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Error ducimus sit beatae.",
      icon: "play"
    },
    {
      imageSrc:"assets/img/logo.png",
      imageAlt: 'logo',
      title: "Escucha tu musica",
      subtitle: "Donde quiera que este",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Error ducimus sit beatae.",
      icon: "play"
    }
  ];

  constructor( private router: Router, private storage: Storage) { }

  ngOnInit() {
  }


  finish() {

    this.storage.set('isIntroShowed', true);
    this.router.navigateByUrl('/login');

  }

}
