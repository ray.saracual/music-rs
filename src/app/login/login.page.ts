import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharedService } from '../services/shared.service';
import { NavController } from '@ionic/angular';
import { Storage} from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  validation_messages = {

    user:[

      {type:"required", message: "El email es requerido"},
      {type:"pattern", message: "Email no válido"}
    ],

    password:[
      {type:"required", message: "El password es requerido"},
      {type:"minlength", message: "Minimo 4 caracteres"}
    ]
  };
  errorLogin: string;

  constructor(private formBuilder: FormBuilder, private sharedService: SharedService,
    private navCrtl: NavController, private storage: Storage) {

    this.loginForm = this.formBuilder.group(
     {
     user: new FormControl(
       '',
        Validators.compose([
       Validators.required,
      //  Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    ]
       )
      ),

      password: new FormControl(
        '',
         Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ]
        )
        ),
  });

  }

  ngOnInit() {}


  loginUser(user){
    this.sharedService.login(user)
    .subscribe(res => {

      // if (res[0].status === 0 && !res[0].status ){
      //   this.errorLogin="Login incorrecto";
      //   return;
      //  }

      //  this.errorLogin ="";
       this.storage.set('key',res.usuario._id);


      // sirve para navegar atras o adelante
      this.navCrtl.navigateForward("/menu/home");
    }, error => {
      this.errorLogin="Login incorrecto";
       console.log(error);
      } );
  }

  goToRegister() {
    this.navCrtl.navigateForward("/register");
  }



}
