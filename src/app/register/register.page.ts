import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharedService } from '../services/shared.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  validation_messages = {

    email:[

      {type:"required", message: "El email es requerido"},
      {type:"pattern", message: "Email no válido"}
    ],

    password:[
      {type:"required", message: "El password es requerido"},
      {type:"minlength", message: "Minimo 4 caracteres"}
    ]
  };
  msgRegister: any;

  constructor(private formBuilder: FormBuilder,
              private sharedService: SharedService,
              private navCrtl: NavController) {

    this.registerForm = this.formBuilder.group(
     {
     email: new FormControl(
       '',
       Validators.compose([
       Validators.required,
       Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]
       )
      ),

      password: new FormControl(
        '',
        Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ]
        )
        ),
  });

  }

  ngOnInit() {
  }


  finish() {
  this.navCrtl.navigateBack('/login');
   }

   register(user) {
    this.sharedService.register(user)
    .subscribe(res => { 

      console.log(res[0].status);




const estatus = res[0].status;

      if (res[0].status === 0) {
      this.msgRegister = 'Error al registrar usuario';
      setTimeout(() => {
      this.msgRegister = '';
      }, 3000);
      return;
      }
      
      this.msgRegister = 'Usuario Registrado';

      setTimeout(() => {
        this.msgRegister = '';
        this.navCrtl.navigateBack('/login');
      }, 2000);

      console.log(res);
    }, error => {

     this.msgRegister = error;
      console.log(error);
    });
   }


}
